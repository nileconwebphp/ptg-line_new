import React from 'react';

const modal = (props) => {
  var actions = props.action
  console.log(actions)
  return (
    <div>

      <div className="modal fade" id="Modal" role="dialog" aria-labelledby="myModalLabel">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id="myModalLabel">ลบ</h4>
            </div>
            <div className="modal-body">
              {props.children}
            </div>
            <br />
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">ไม่</button>
              <button type="button" name="savechane" className="btn btn-primary" onClick={this.handleDelete}>ใช่</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default modal;