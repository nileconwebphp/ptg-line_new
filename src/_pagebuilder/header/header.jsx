import React from "react";
import { Redirect } from "react-router";
import { NavLink } from "react-router-dom";
import "./header.css";

export class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logout: false
    };
  }

  render() {
    const { logout } = this.state;
    if (logout) {
      localStorage.clear();
      return <Redirect to="/login" push={true} />;
    }

    return (
      <div className="navbar navbar-default header-highlight">
        <div className="navbar-header">
          <NavLink to="/homepage" className="navbar-brand">
          <img src="/../images/logo.png" alt="spar" className="logo-login"></img>
          </NavLink>
          <ul className="nav navbar-nav visible-xs-block">
            <li>
              <a
                to="/posts"
                data-toggle="collapse"
                data-target="#navbar-mobile"
              >
                <i className="icon-tree5" />
              </a>
            </li>
            <li>
              <a to="/posts" className="sidebar-mobile-main-toggle">
                <i className="icon-paragraph-justify3" />
              </a>
            </li>
          </ul>
        </div>

        <div className="navbar-collapse collapse" id="navbar-mobile">
          <ul className="nav navbar-nav">
            <li>
              <a
                to="/posts"
                className="sidebar-control sidebar-main-toggle hidden-xs"
              >
                <i className="icon-paragraph-justify3" />
              </a>
            </li>
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li className="dropdown dropdown-user">
              <a to="/posts" className="dropdown-toggle" data-toggle="dropdown">
                <img src="/assets/images/placeholder.jpg" alt="spar" />
                <span>Admin</span>
                <i className="caret" />
              </a>

              <ul className="dropdown-menu dropdown-menu-right">
                <li>
                  <a onClick={() => this.setState({ logout: true })}>
                    <i className="icon-switch2" /> Logout
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
