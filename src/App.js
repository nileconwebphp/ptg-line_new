import React from "react";
import { withRouter } from 'react-router-dom';
import Routing from "./routes";

class App extends React.Component {
  render() {
    return (
      <div className="my-app">
        <Routing />
      </div>
    );
  }
}

export default withRouter(App);
