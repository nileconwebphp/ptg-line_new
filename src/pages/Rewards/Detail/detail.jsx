import React from "react";
import "../rewards.css";
import Rating from "react-rating";
import SweetAlert from "react-bootstrap-sweetalert";
import { promotionActions } from "../../../_actions";
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';

var crypto = require('crypto');
var ButtonType = [];
var timeFirst = 0
var setTimestamp;
var msg = "ไม่สามารถดำเนินการได้"
var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_danger2.jpg`;
var imgWarning = `${process.env.PUBLIC_URL}/images/info.png`;


export class RewardDetail extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            fields: {},
            show: false,
            modal: null,
            getDetail: { 
                imG_URL_DEFAULT : '',
                redeeM_CODE: ''
            },
            showLoading: 'block',
            content_page: 'none',
            test : '',
            timeCountLast: '',
            noteQuatity: '',
            checked : false,

            checkTypeEstamp: false,
        }; 

        // this.checkUnixTime = this.checkUnixTime.bind(this);
        this.RedeemReward = this.RedeemReward.bind(this);
    }

    

    componentDidMount() {
        const params = this.props.match.params;
        this.setState({
            rewardsId: params.rewardsId
        });

        this.decryptAction(sessionStorage.getItem('reqData'), params.rewardsId);
        // this.handleChangeButton(params.rewardsId)
    }

    decryptAction(subToken, promotionid){
        var tokenencode = decodeURIComponent(subToken)
        var showLoading = ''
        var content_page = ''
        let { requestData } = this.state; 
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
        const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
        var decipher = crypto.createDecipheriv('aes-256-cbc', ENC_KEY, IV);
        var dec = decipher.update(tokenencode,'base64','utf-8');
        dec += decipher.final('utf-8'); 
        var convertData = JSON.parse(dec); 

        this.getCardList(convertData, promotionid);
        this.setState({ requestData : convertData })
    }

    getCardList(data, promotionid){
        var tokenId = data.mobileToken;
        var cusId = data.mobileCusID;
        var showLoading = ''
        var content_page = ''
        promotionActions.getCarcard(cusId,tokenId).then(e =>{
            // console.log("getCarcard", e)
            if(e.data.isSuccess === true){
                showLoading = 'none'
                content_page = 'block'
                this.setState({ cardType : e.data.data[0] })
                this.getDetailPromotion(promotionid, e.data.data[0])
            }else{
                showLoading = 'none'
                content_page = 'block'
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    handleSelectSend() {
        this.setState({ shows: true })
    }

    insertRedeemPromotion(data, urlWebView){
        var showLoading = '';
        var content_page = '';
        var dataRedeem = {
            TOKEN_ID: data.mobileToken,
            CUSTOMER_ID: data.mobileCusID,
            CARD_MASTER_ID: data.mobileCardMasterID,
            SHIPPING_TYPE: '4',
            SHIPPING_SHOP_ID: '',
            REDEEM_CODE: sessionStorage.getItem('redeem_code'),
            FullAddress:  '',
            REDEEM_DETAIL:'',
            SOURCE_DATA: 19
        }
        
        promotionActions.insertRedeem(dataRedeem).then( e => {
            if(e.data.isSuccess === true){
                var maxCardId = data ? data.mobileCardNo : ""
                var customerId = data.mobileCusID != undefined ? data.mobileCusID : ""
                var citizeN_ID = data.citizeN_ID != undefined ? data.citizeN_ID : ""
                var phoneNo = data.phoneNo != undefined ? data.phoneNo : ""
                var email = data.USER_EMAIL != undefined ? data.USER_EMAIL : ""
                var TokenId = data.mobileToken != undefined ? data.mobileToken : ""
                var dataparams = `{
                    "customerId" : "${customerId}",
                    "IdCard" : "${citizeN_ID}",
                    "maxCardId" : "${maxCardId}",
                    "telNo" : "${phoneNo}",
                    "email" : "${email}",
                    "TokendID" : "${TokenId}",
                    "TransID" : "${e.data.data[0].tranS_ID}",
                    "ecode" : "${e.data.data[0].redeeM_PARTNER_CODE}"
                }`

                const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019";
                const IV = "xRtyUw5Pt+58ZxqA";
                let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
                let encrypted = cipher.update(dataparams);
                encrypted = Buffer.concat([encrypted, cipher.final()]);
                var setParam = encrypted.toString('base64');
                var link = urlWebView.replace('#param#', setParam)
                window.location.href=link
                // var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_correct1@3x.png`;
                // this.modalCheckSubmit(e.data.errMsg,imgPopup)
            }else{
                this.setState({ msg : e.data.errMsg })
                var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_danger2.jpg`;
                this.modalCheckSubmit(e.data.errMsg,imgPopup)
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },1000)
        })
    }

    getDetailPromotion(promotionid, cardType){
        let { noteQuatity } = this.state;
        if(this.state.getDetail.imG_URL_DEFAULT === ''){
            this.state.getDetail.imG_URL_DEFAULT = ''
        }
        console.log(cardType);
        promotionActions.getDetailPromotionByID(promotionid).then(e => {
            console.log("getDetailPromotionByID", e)
            var showLoading = ''
            var content_page = ''
            if(e.data.isSuccess === true){
                sessionStorage.setItem('redeem_code' , e.data.data.redeeM_CODE)
                sessionStorage.setItem('img' , e.data.data.imG_URL_NORMAL)
                sessionStorage.setItem('redeem_name' , e.data.data.redeeM_NAME )
                sessionStorage.setItem('total_point',e.data.data.redeeM_TOTAL_POINT)
                sessionStorage.setItem('rating_star' , e.data.data.ratingStar)
                this.setState({ getDetail : e.data.data })
                
                if(e.data.data.redeeM_POINT_TYPE == 5){
                    this.setState({ 
                        checkTypeEstamp: true,
                    })

                    // if(Math.floor(cardType.carD_ESTAMP) < Math.floor(e.data.data.redeeM_STAMP)){
                    //     this.setState({ checked : true})
                    // }  
                } else {
                    this.setState({ 
                        checkTypeEstamp: false,
                    })
                }
                showLoading = "none";
                content_page = "block";
                var res = e.data.data
                if(e.data.data.quantitY_REMAIN === 0 || e.data.data.redeeM_STATUS !== "Y"){
                    noteQuatity = <div> {'ขออภัยของรางวัลที่ท่านเลือกหมดแล้ว'} <br /> {'กรุณาเลือกรายการอื่น'}</div>
                    this.setState({ noteQuatity , checked : true})
                }       
            }else{
                showLoading = "block";
                content_page = "none";
            }

            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },1500)
        })
    }

    // handleChangeButton(rewardsId){
    //     if(rewardsId === '1'){
    //         ButtonType.push(
    //             <a className="btn btn-block btn-green t-22" onClick={() => this.handleSelectSend()} > แลกคะแนน </a>
    //         );
    //     } else {
    //         ButtonType.push(
    //             <a className="btn btn-block btn-green t-22" href="/line/rewards/barcode"> แลกคะแนน </a>
    //         );
    //     }
    // }

    modalCheckRedeem(res, img, type) {
        alert = (
            <SweetAlert
            custom
            showCancel
            showCloseButton
            confirmBtnText={"ยืนยัน"}
            cancelBtnText={"ยกเลิก"}
            confirmBtnBsStyle="success"
            cancelBtnBsStyle="light"
            // closeOnClickOutside={false}
            // focusConfirmBtn={false}
            // title=""
            customIcon={img}
            showConfirm={true}
            showCancel={true}
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true, type)}
            reverseButtons={true}
          >
            <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
            <div className="fontSizeCase">{res}</div>
          </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }

    handleChoice(choice, type) {
        const { getDetail, requestData } = this.state

        if (choice === false) {
            this.setState({ show: false , modal: null })
        }else{
            if(type == 'insert'){
                this.setState({ show: false , modal: null })
                this.RedeemReward() // CHECK ไปแลกรางวัล    
            }else if(type == 'gopage'){
                this.setState({ show: false , modal: null })
                window.location.href = `/line/rewards/barcode/${getDetail.redeeM_CODE}`;  
            }
        }   
    }

    checkRedeem(){
        const { getDetail, requestData } = this.state
        var urlWebView = getDetail.urlWebView
        if(urlWebView){
            this.modalCheckRedeem("ยืนยันใช้คะแนนแลกของรางวัล ?",imgWarning, 'insert')
        }else{
            let shippinG_TYPE = getDetail.shippinG_TYPE
            this.checkGotoRedeem(shippinG_TYPE)
        }
    }

    checkGotoRedeem(shippinG_TYPE){
        // console.log("getDetail",getDetail)
        var timeLast = Math.round((new Date()).getTime()); 
        var timeCount = parseInt(sessionStorage.getItem('timeFirst'))

        if(timeCount > timeLast){
            if(shippinG_TYPE == 1){
                setTimestamp = true
                sessionStorage.setItem('unixtime',timeLast)
                window.location.href = `/line/rewards/branch/${this.state.rewardsId}`;
            } else if(shippinG_TYPE == 2){
                setTimestamp = true
                sessionStorage.setItem('unixtime',timeLast)
                window.location.href = `/line/rewards/address/${this.state.rewardsId}`;
            } else if(shippinG_TYPE == 3){
                setTimestamp = true
                sessionStorage.setItem('unixtime',timeLast)
                this.modalCheckRedeem("ยืนยันใช้คะแนนแลกของรางวัล ?",imgWarning, 'gopage')
            }
        }else{     
            this.modalCheckSubmit(msg,imgPopup)
        }
    }

    RedeemReward(){
        const { getDetail, requestData } = this.state
        var urlWebView = getDetail.urlWebView
        this.insertRedeemPromotion(requestData, urlWebView)
    }

    clickInterest(){
        const { getDetail, requestData } = this.state
        // console.log(requestData);
        var maxCardId = requestData == "" ? "" : requestData.mobileCardNo
        var customerId = requestData == "" ? "" : requestData.mobileCusID
        var citizeN_ID = requestData == "" ? "" : requestData.citizeN_ID
        var phoneNo = requestData == "" ? "" : requestData.phoneNo
        var email = requestData == "" ? "" : requestData.USER_EMAIL
        var TokenId = requestData == "" ? "" : requestData.mobileToken
        var data = `{
            "customerId" : "${customerId}",
            "IdCard" : "${citizeN_ID}",
            "maxCardId" : "${maxCardId}",
            "telNo" : "${phoneNo}",
            "email" : "${email}",
            "TokendID" : "${TokenId}"
        }`
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019";
        const IV = "xRtyUw5Pt+58ZxqA";
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
        let encrypted = cipher.update(data);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        var setParam = encrypted.toString('base64');
        var urlWebView = getDetail.urlWebView
        var link = urlWebView.replace('#param#', setParam)
        window.location.href=link
    }

    // checkUnixTime(){        
    //     var timeLast = Math.round((new Date()).getTime()); 
    //     // var timeLast = Math.round((new Date()).getTime());  
    //     var timeCount = parseInt(sessionStorage.getItem('timeFirst'))
    //     if(timeCount > timeLast){
    //         setTimestamp = true
    //         sessionStorage.setItem('unixtime',timeLast)
    //         window.location.href = `/line/rewards/barcode/${this.state.getDetail.redeeM_CODE}`;

    //     }else{     
    //         this.modalCheckSubmit(msg,imgPopup)
    //     }
    // }

    modalCheckSubmit(res,img){
        alert = (
          <SweetAlert
            custom
            confirmBtnBsStyle="success"
            closeOnClickOutside={true}
            cancelBtnBsStyle="default"
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
            onOutsideClick={() => {
              this.wasOutsideClick = true;
              this.setState({ showConfirm: false })
            }}
          >
            <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
            <div className="res-popup">{res}</div>
          </SweetAlert>
        );
    
        this.setState({ show: true , modal: alert })
      }

    render() {
        const { checkTypeEstamp } = this.state
        return (
            <div> 
                <div id="loading" style={{display : this.state.showLoading}}>
                    <div className="content_class">
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                </div>
                {/* <SweetAlert 
                    show={this.state.shows}
                    showConfirm={false}
                    showCancel	={false}
                    customClass= "sweet-myshop"
                >
                        <div className="text-right pr-2">
                            <img 
                                src={process.env.PUBLIC_URL +"/images/icon/cross_feed@3x.png"} 
                                width="25"
                                alt=""
                                onClick={() => this.setState({ shows: false })} 
                            /> 
                        </div>
                        <div className="pr-3 csweet-myshop">
                            <h3 className="mt-3 mb-4">เลือกวิธีการจัดส่ง</h3>
                            <a href="/line/rewards/address" className="btn btn-block btn-selectsend">จัดส่งตามที่อยู่ปัจุบัน</a>
                            <a href="/line/rewards/branch" className="btn btn-block btn-selectsend">จัดส่งที่สาขา</a>
                        </div>
                </SweetAlert> */}
                <div className="content_page" style={{ display: this.state.content_page}}>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12 p-0">
                            <img 
                                src={this.state.getDetail.imG_URL_DEFAULT}
                                className="img-fluid w-100"
                                alt="ptg"
                            />
                        </div>
                    </div>
                </div>
                <div className="container de-content mt-3">
                    <h4 className="t-green">{this.state.getDetail.redeeM_NAME}</h4>
                    <div>{ReactHtmlParser(this.state.getDetail.redeeM_DESC)}</div>
                    <div className="d-flex pt-3">
                        <div className="mr-auto">
                            <Rating
                                {...this.props} 
                                initialRating={this.state.rate}
                                placeholderRating={this.state.getDetail.ratingStar}
                                emptySymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargray.png"} className="icon" width="15"/>}
                                placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargreen.png"} className="icon" width="15"/>}
                                readonly
                            />
                            <span className="px-2">(0)</span>
                            <div className="font-color-red pt-3">{this.state.noteQuatity !== '' ? this.state.noteQuatity : ''}</div>
                        </div>
                        <div className="">
                            <button type="button" className="btn btn-greenhalf txth-20 px-4">แลกแต้ม <br/> {checkTypeEstamp ? this.state.getDetail.redeeM_STAMP : this.state.getDetail.redeeM_TOTAL_POINT}</button>
                        </div>
                    </div>
                    <hr/>
                    <div>{ ReactHtmlParser(this.state.getDetail.redeeM_CONDITION) }</div>
                </div>
                <div className="container">
                    <div className="heigh-footer">
                    <div className="row fix-bottom">
                        <div className="col-12">
                            {
                                this.state.getDetail.redeeM_CODE  == null || this.state.getDetail.redeeM_CODE == ""
                                ? 
                                    <button 
                                        className="btn btn-block btn-green t-22" 
                                        onClick={() => this.clickInterest()}
                                    > 
                                        สนใจคลิก
                                    </button>
                                : 
                                    <button 
                                        className="btn btn-block btn-green t-22" 
                                        onClick={() => this.checkRedeem()}
                                        disabled={this.state.checked}
                                    > 
                                        แลกคะแนน
                                    </button>
                            }
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            {this.state.modal}

            </div>
        );
    }
}
