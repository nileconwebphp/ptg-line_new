import axios from "axios"
import { Base_API } from "../_constants/matcher"

export const promotionActions = {
    getCateList,
    getPromotionAll,
    getDetailPromotionByID,
    insertRedeem,
    decrypt_,
    encrypt_,
    confirmLineReward,
    getCarcard,
    getCardList,
    GetPromotion_EStamp,
    GetCustomerProfile,
    getProvinceNew,
    GetPTBranch
}

function getCateList(){
    return axios.get(`${Base_API.promotion}/GetPromotionCatalog`).then(res => {
        return res.data;
    })
}

function getPromotionAll(pageno,txtsearch,catecode,cateType){
    return axios.get(`${Base_API.promotion}/GetPromotionList?lang=TH&cardType=${cateType}&categoryId=${catecode}&pageNo=${pageno}&textsearch=${txtsearch}`).then(res => {
        return res;
    })
}

function getDetailPromotionByID(promotionid){
    return axios.get(`${Base_API.promotion}/GetPromotionById?language=th&promotionID=${promotionid}`).then(res => {
        return res;
    })
}

function insertRedeem(data){
    return axios.post(`${Base_API.webview}/Line_InsertRedeemPromotion` , data).then(res => {
        return res;
    })
}

function decrypt_(subToken){
    let formData = new FormData();
    formData.append('ciphertext' , subToken)
    return axios.post(`${Base_API.webview}/_decrypt` , formData).then(res => {
        return res;
    })
}

function encrypt_(data){
    let formData = new FormData();
    formData.append('plaintext' , data)
    return axios.post(`${Base_API.webview}/_Encrypt` , formData).then(res => {
        return res;
    })
}

function confirmLineReward(data){
    let formData = new FormData();
    formData.append('jsonReq' , data)
    return axios.post(`${Base_API.linePush}/linePushMessage/linePushMessageReward` , formData).then(res => {
        return res;
    })
}

function getCarcard(cusId,tokenId){
    return axios.get(`${Base_API.apis}/webview/CardList?customerId=${cusId}&tokenId=${tokenId}`).then(res => {
      return res;
    })
  }

  function getCardList(userid){
      return axios.get(`${Base_API.apis}/PTMaxCard/CardList?userId=${userid}`).then(res => {
        return res;
      })
}

function GetPromotion_EStamp(cardType, categoryId, pageNo){
    return axios.get(`${Base_API.apis}/Promotion/GetPromotion_EStamp?lang=th&cardType=${cardType}&categoryId=${categoryId}&pageNo=${pageNo}`).then(res => {
        return res;
    })
}

function GetCustomerProfile(formData){
    return axios.post(`${Base_API.apis}/WebView/GetCustomerProfile` , formData).then(res => {
      return res;
    })
  }


  function getProvinceNew(){
    return axios.post(`${Base_API.apis}/WebView/GetDropDownPROVINCE`).then(res => {
      return res;
    })
  }

  function GetPTBranch(provinceCode){
      return axios.get(`${Base_API.apis}/Promotion/GetPTBranch?provinceCode=${provinceCode}`).then(res => {
          return res;
      })
  }