import React from "react";
import "../rewards.css";
import Rating from "react-rating";
import QRCode from 'qrcode.react';
import Barcode  from 'react-barcode';
import { promotionActions } from "../../../_actions";
import SweetAlert from "react-bootstrap-sweetalert";
import moment from 'moment-timezone';

var redeeM_PARTNER_CODE = ''
var data = {};
var crypto = require('crypto');

export class RewardBarcode extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            fields: {},
            getBarcode: {},
            requestData: {},
            show: false,
            modal: null,
            showLoading: 'block',
            content_page: 'none',
        }; 
    }

    componentDidMount(){        
        if(redeeM_PARTNER_CODE === undefined){
            this.state.getBarcode.data[0].redeeM_PARTNER_CODE = ''
        }
        if(this.state.requestData === undefined){
            data = ''
        }
        this.decryptAction(sessionStorage.getItem('reqData'));
    }

    decryptAction(subToken){
        console.log(subToken)
        var tokenencode = decodeURIComponent(subToken)
        let { requestData } = this.state; 
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
        const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
        var decipher = crypto.createDecipheriv('aes-256-cbc', ENC_KEY, IV);
        var dec = decipher.update(tokenencode,'base64','utf-8');
        dec += decipher.final('utf-8'); 
        var convertData = JSON.parse(dec);
        this.setState({ requestData : convertData })
        this.insertRedeemPromotion(convertData)
    }

    insertRedeemPromotion(data){
        var showLoading = '';
        var content_page = '';
        var dataRedeem = {
            TOKEN_ID: data.mobileToken,
            CUSTOMER_ID: data.mobileCusID,
            CARD_MASTER_ID: data.mobileCardMasterID,
            SHIPPING_TYPE: '4',
            SHIPPING_SHOP_ID: '',
            REDEEM_CODE: sessionStorage.getItem('redeem_code'),
            FullAddress:  '',
            REDEEM_DETAIL:'',
            SOURCE_DATA: 19
        }
        promotionActions.insertRedeem(dataRedeem).then( e => {
            if(e.data.isSuccess === true){
                showLoading = 'none';
                content_page = 'block';
                this.setState({ getBarcode : e.data })
                this.encryptAction(e.data.data,data)
                // var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_correct1@3x.png`;
                // this.modalCheckSubmit(e.data.errMsg,imgPopup)
            }else{
                this.setState({ msg : e.data.errMsg })
                var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_danger2.jpg`;
                this.modalCheckSubmit(e.data.errMsg,imgPopup)
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },1000)
        })
    }


  modalCheckSubmit(res,img){
    alert = (
      <SweetAlert
        custom
        confirmBtnBsStyle="success"
        closeOnClickOutside={true}
        cancelBtnBsStyle="default"
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
        onOutsideClick={() => {
          this.wasOutsideClick = true;
          this.setState({ showConfirm: false })
        }}
      >
        <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
        <div className="res-popup">{res}</div>
      </SweetAlert>
    );

    this.setState({ show: true , modal: alert })
  }

  handleChoice(choice) {
    if (choice === false) {
        this.setState({ show: false , modal: null })
        window.location.href = `/line/rewards/detail1/${sessionStorage.getItem('pr_id')}`
    }
  }

    encryptAction(dataapi,datadecrypt){
        var setUnix = parseInt(sessionStorage.getItem('unixtime'))
        var dataSend = `{
            "lineID":"${datadecrypt.lineID}",
            "transID":${dataapi[0].tranS_ID},
            "redeemCode":"${dataapi[0].redeeM_CODE}",
            "redeemPartnerCode":"${dataapi[0].redeeM_PARTNER_CODE}",
            "expirePartnerCode":"${dataapi[0].expirE_PARTNER_CODE}",
            "unixTime":${setUnix}
        }`
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
        const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
        let encrypted = cipher.update(dataSend);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        var en = encrypted.toString('base64');

        sessionStorage.setItem('dataShow' , dataapi[0].redeeM_PARTNER_CODE);

        // promotionActions.encrypt_(dataSend).then(res => {
        //     sessionStorage.setItem('dataShow' , dataapi[0].redeeM_PARTNER_CODE) 
        //     this.PushMessageReward(res)
        // })
        this.PushMessageReward(en)    
    }

    PushMessageReward(res){
        promotionActions.confirmLineReward(res).then(e => {
            if(e.data.response.resCode === "00"){
                console.log(true)
            }else{
                console.log(false)
            }
        })
    }

    render() {
        return (
            <div> 
            <div id="loading" style={{display : this.state.showLoading}}>
                    <div className="content_class">
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                </div>
            <div className="container content_page de-content py-3 px-3" style={{ display: this.state.content_page}}>
                <div className="rewards-card">
                    <div className="row">
                        <div className="col-5 pr-0">
                            <img 
                                src={sessionStorage.getItem('img')}
                                className="img-fluid w-100"
                                alt="ptg"
                            />
                            {/* <div className="recards-badge">ใหม่</div> */}
                        </div>
                        <div className="col-7 d-flex align-items-start flex-column pt-3 pr-4">
                            <div className="mb-auto t-green t-limit-3 col1-recards-txt">{sessionStorage.getItem('redeem_name')} โดยมีรายละเอียดดังนี้</div>
                            <div className="row pt-5">
                                <div className="col-6 vote">
                                    <Rating
                                        {...this.props} 
                                        initialRating={this.state.rate}
                                        placeholderRating={sessionStorage.getItem('rating_star')}
                                        emptySymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargray.png"} className="col1-recards-rate"/>}
                                        placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargreen.png"} className="col1-recards-rate"/>}
                                        readonly
                                    />
                                        <span className="t-green col1-recards-rate-txt my-auto">(0)</span>
                                </div>
                                <div className="col-6 text-right point">
                                    <button type="button" className="btn col1-recards-btn-greenhalf">
                                        <div className="">แลกแต้ม </div>
                                        <div className="">{sessionStorage.getItem('total_point')}</div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div className="row text-center px-3">
                    <div className="col-12">
                        <ul className="nav nav-pills coupon-ul justify-content-center">
                            <li className="nav-item coupon-li">
                                <a className="nav-link coupon-a active mx-2" data-toggle="pill" href="#qrcode">QR code</a>
                            </li>
                            <li className="nav-item coupon-li">
                                <a className="nav-link coupon-a mx-2" data-toggle="pill" href="#barcode">Bar code</a>
                            </li>
                        </ul>
                        <div className="tab-content py-4 text-center">
                            <div className="tab-pane container active" id="qrcode">
                                <h3 className="t-gray t-bold t-28">{sessionStorage.getItem('dataShow')}</h3>
                                <QRCode value={`${sessionStorage.getItem('dataShow')}`}/>
                            </div>
                            <div className="tab-pane container fade" id="barcode">
                                <div className="row">
                                    <div className="col-12">
                                        <h3 className="t-gray t-bold t-28">{sessionStorage.getItem('dataShow')}</h3>
                                        <Barcode value={`${sessionStorage.getItem('dataShow')}`}/>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-12 p-0 t-20 t-gray">
                        <div className="">วิธีการใช้งาน</div>
                    </div>
                    <div className="col-12 p-0 pl-1 t-20 t-gray">
                        <div className="txth-20">การใช้แลกรับสินค้า สามารถเลือกรับสินค้าตามสาขาที่เลือก และเมื่อกดแลกสินค้าแล้ว ทางบริษัทจะจัดส่งสินค้าตามสาขาที่ระบุ</div>
                    </div>
                </div>
            </div>
            {/* <div className="container fix-bottom py-3">
                <div className="row">
                    <div className="col-12">
                        <a className="btn btn-block btn-greendark t-22" href="/line/rewards/">กลับไปหน้าหลัก</a>
                    </div>
                </div>
            </div> */}
            {this.state.modal}
        </div>
        );
    }
}
