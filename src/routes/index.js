import React from 'react'
import { Switch, Route } from 'react-router-dom'
// import { PrivateRoute } from '../_components'

import NotFoundPages from '../pages/404'

import { Rewards } from '../pages/Rewards/List'
import { RewardDetail } from '../pages/Rewards/Detail'
import { RewardBarcode } from '../pages/Rewards/Barcode'  
import { RewardAdress } from '../pages/Rewards/Address'
import { RewardAdressSuccess } from '../pages/Rewards/Address/RewardAdressSuccess'
import { RewardBranch} from '../pages/Rewards/Branch/List'
import { RewardBranchSearch} from '../pages/Rewards/Branch/Search'
import { RewardBranchExchange} from '../pages/Rewards/Branch/Exchange'
import { checkLine } from '../pages/checkLine'
import { RewardsEStamp } from '../pages/Rewards/EStamp'

import { Register } from '../pages/Register'

export default () => (
  <Switch>
    {/* <PrivateRoute component={NotFoundPages} /> */}
    
    <Route exact path="/line/rewards/token" component={Rewards}/>
    <Route exact path="/line/rewards/checkLine" component={checkLine}/>
    <Route exact path="/line/rewards/detail1/:rewardsId" component={RewardDetail} />
    <Route exact path="/line/rewards/detail2/:rewardsId" component={RewardDetail} />
    <Route exact path="/line/rewards/barcode/:rewardsId" component={RewardBarcode} />
    <Route exact path="/line/rewards/address/:rewardsId" component={RewardAdress} />
    <Route exact path="/line/rewards/address/success/:rewardsId" component={RewardAdressSuccess} />
    <Route exact path="/line/rewards/branch/:rewardsId" component={RewardBranch} />
    <Route exact path="/line/rewards/branch/search/:rewardsId" component={RewardBranchSearch} />
    <Route exact path="/line/rewards/branch/exchange/:rewardsId" component={RewardBranchExchange} />
    <Route exact path="/line/rewards/estamp" component={RewardsEStamp}/>

    <Route exact path="/line/register" component={Register} />

    <Route component={NotFoundPages} />

  </Switch>
)
