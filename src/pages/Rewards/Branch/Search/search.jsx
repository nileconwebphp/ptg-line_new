import React from "react";
import "../../rewards.css";
import FilterResults from 'react-filter-search';
import SweetAlert from "react-bootstrap-sweetalert";
import { promotionActions } from "../../../../_actions"; 
var crypto = require('crypto');

var imgWarning = `${process.env.PUBLIC_URL}/images/info.png`;

export class RewardBranchSearch extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            fields: {},
            showLoadingDetail: 'none',
            ProvineType: '',
            failed: false,
            getProvince: {},
            GetPTBranch: [],
            branchName: "",
            requestData: "",
            BranchSearchvalue: '',
        }; 
    }

    componentDidMount() {
        const params = this.props.match.params;
        this.setState({
            rewardsId: params.rewardsId
        });
        this.decryptAction(sessionStorage.getItem('reqData'), params.rewardsId);
    }

    decryptAction(subToken, promotionid){
        console.log(subToken)
        var tokenencode = decodeURIComponent(subToken)
        let { requestData } = this.state; 
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
        const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
        var decipher = crypto.createDecipheriv('aes-256-cbc', ENC_KEY, IV);
        var dec = decipher.update(tokenencode,'base64','utf-8');
        dec += decipher.final('utf-8'); 
        var convertData = JSON.parse(dec);
        this.setState({ requestData : convertData })
        this.getProvince()
    }

    getProvince(){
        var showLoading = ''
        var content_page = ''
        promotionActions.getProvinceNew().then(e => {
            console.log("getProvinceNew", e)
            var resp = e.data.dropdowN_INFO
            showLoading = 'none'
            content_page = 'block'
            this.setState({ getProvince : resp })
            var th = this
            setTimeout(function(){
                    th.setState({ showLoading : showLoading})
                    th.setState({ content_page: content_page })
            },2000)
        })
    }

    GetPTBranch = (e) => {
        const value = e.target.value;
        this.setState({ 
            showLoadingDetail: 'block',
            Province: value,
            BranchSearch: false,
            BranchSearchvalue: ""
        })
        promotionActions.GetPTBranch(value).then(e => {
            console.log("GetPTBranch", e)
            var resp = e.data.data
            this.setState({ 
                GetPTBranch : resp,
                showLoadingDetail: 'none'
            })
        })
    } 

    handleChangeSelect(e) {
        var value = e.target.value;
        this.setState({province: value});
        this.setState({ showLoadingDetail: 'block' })
        setTimeout(() => {
              this.setState({ showLoadingDetail: 'none' })
        }, 1500);
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({ submitted: true });
        this.handleProvince()
    }

    handleChange = (e) => {
        console.log("e",e)
        const branchName = e.target.value;
        const branchAddress = e.target.dataset.address;
        const branchId = e.target.id;
        this.setState({ 
            branchName: branchName, 
            branchAddress: branchAddress,
            branchId: branchId
        });
    };

    validateForm(){
        let errors = {};
        let errorsFocus = {};
        let formIsValid = true;
        const { ProvineType } = this.state;

        if(!ProvineType){
            formIsValid = false;
            this.handleFailed()
        }
        this.setState({
            errors: errors,
            errorsFocus: errorsFocus
        })
        return formIsValid;
    }

    handleProvince(){
        if(this.validateForm()){
            const { ProvineType } = this.state;
            localStorage.setItem('ProvineType', ProvineType);
            window.location.href="/line/rewards/branch/exchange"
        }
    }
   
    handleFailed = () => {
        this.setState({ failed: true })
    }

    onConfirmtoFailed  () {
        this.setState({ failed: false })
    }

    modalCheckRedeem(res, img) {
        alert = (
            <SweetAlert
            custom
            showCancel
            showCloseButton
            confirmBtnText={"ยืนยัน"}
            cancelBtnText={"ยกเลิก"}
            confirmBtnBsStyle="success"
            cancelBtnBsStyle="light"
            // closeOnClickOutside={false}
            // focusConfirmBtn={false}
            // title=""
            customIcon={img}
            showConfirm={true}
            showCancel={true}
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
            reverseButtons={true}
          >
            <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
            <div className="fontSizeCase">{res}</div>
          </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }

    InsertRedeemPromotion(){
        this.setState({ showLoading: 'block' })
        const { requestData, branchAddress, branchId, branchName } = this.state;
        var showLoading = ''
        var content_page = ''
        var dataRedeem = {
            TOKEN_ID: requestData.mobileToken,
            CUSTOMER_ID: requestData.mobileCusID,
            CARD_MASTER_ID: requestData.mobileCardMasterID,
            SHIPPING_TYPE: '1',
            SHIPPING_SHOP_ID: branchId,
            REDEEM_CODE: sessionStorage.getItem('redeem_code'),
            FullAddress:  '',
            REDEEM_DETAIL:'',
            SOURCE_DATA: 19
        }
        console.log("dataRedeem",dataRedeem)
        promotionActions.insertRedeem(dataRedeem).then( e => {
            console.log("insertRedeem",e)
            if(e.data.isSuccess === true){
                showLoading = 'none';
                content_page = 'block';
                var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_correct1@3x.png`;
                this.modalCheckSubmitSuccess("แลกของรางวัลสำเร็จ",imgPopup)
                sessionStorage.setItem('branchAddress',branchAddress)
                sessionStorage.setItem('branchName',branchName)
            }else{
                this.setState({ msg : e.data.errMsg })
                var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_danger2.jpg`;
                this.modalCheckSubmit(e.data.errMsg,imgPopup)
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading })
                th.setState({ content_page: content_page })
            },1000)
        })
    }
    
    modalCheckSubmitSuccess(res,img){
        alert = (
          <SweetAlert
            custom
            confirmBtnBsStyle="success"
            closeOnClickOutside={true}
            cancelBtnBsStyle="default"
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoiceSuccess(false)}
            onConfirm={() => this.handleChoiceSuccess(true)}
            onOutsideClick={() => {
              this.wasOutsideClick = true;
              this.setState({ showConfirm: false })
            }}
          >
            <div className="iconClose" onClick={() => this.handleChoiceSuccess(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
            <div className="res-popup">{res}</div>
          </SweetAlert>
        );
    
        this.setState({ show: true , modal: alert })
      } 

      handleChoiceSuccess(choice) {
          const { rewardsId } = this.state
        if (choice === false) {
            this.setState({ show: false , modal: null })
            window.location.href = `/line/rewards/branch/exchange/${this.state.rewardsId}`
        }
      }

    modalCheckSubmit(res,img){
        alert = (
          <SweetAlert
            custom
            confirmBtnBsStyle="success"
            closeOnClickOutside={true}
            cancelBtnBsStyle="default"
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
            onOutsideClick={() => {
              this.wasOutsideClick = true;
              this.setState({ showConfirm: false })
            }}
          >
            <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
            <div className="res-popup">{res}</div>
          </SweetAlert>
        );
    
        this.setState({ show: true , modal: alert })
      }

      handleChoice(choice) {
        if (choice === false) {
            this.setState({ show: false , modal: null })
        }else{
            this.setState({ show: false , modal: null })
            this.InsertRedeemPromotion() // CHECK ไปแลกรางวัล
        }   
    }


    GetPTBranchSearch = event => {
        const { value } = event.target;
        this.setState({ 
            BranchSearchvalue: value, 
            BranchSearch: true,
        });
    };

    render() {
        const { getProvince, GetPTBranch, modal, BranchSearchvalue } = this.state;
        var getProvinceCard = [];
        for(var p in getProvince){
            getProvinceCard.push(
                <option value={getProvince[p].codE_GUID}>{getProvince[p].values}</option>
            )
        }
        var GetPTBranchCard = [];
        for(var x in GetPTBranch){
            GetPTBranchCard.push(
                <div>
                    <input type="radio" data-address={GetPTBranch[x].address} name="branchName" value={GetPTBranch[x].shoP_NAME} id={GetPTBranch[x].orG_SHOP_ID} />
                    <label className="list-group-item search-listgitem" for={GetPTBranch[x].orG_SHOP_ID}>
                        <div className="t-20 t-greenwhite">{GetPTBranch[x].shoP_NAME}</div>
                        <div className="t-18 t-gray t-limit-2 txth-20">{GetPTBranch[x].address}</div>
                    </label>
                </div>
            )
        }
        return (
            <div> 
                {modal}
                <SweetAlert 
                    show={this.state.failed}
                    confirmBtnCssClass="btn-block btn-lg btn-sweetalert-danger"
                    confirmBtnText="ตกลง"
                    title={<span className="text-26">การแจ้งเตือน</span>} 
                    onConfirm={() => this.onConfirmtoFailed()}
                >
                    <span>
                        <img 
                            src={process.env.PUBLIC_URL +"/images/icon/icon_danger2.jpg"} 
                            className="py-3" 
                            alt="sena"
                            width="70"
                        />
                        <br></br>
                        <span className="t-24">กรุณาเลือกสาขาที่ต้องการรับสินค้า</span>
                    </span>
                </SweetAlert>
                    <div className="container de-content py-3">
                        <div className="row text-center ">
                            <div className="col-12">
                                <div className="input-group mb-3">
                                    <select 
                                        className="form-control form-control-lg bsearch-select arrow-down-black" 
                                        id="selected"
                                        name="province"
                                        onChange={(e) => this.GetPTBranch(e)}
                                        value={this.state.province} 
                                    >
                                        <option disabled selected hidden >เลือกจังหวัด</option>
                                        {getProvinceCard}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <input 
                                                            className="form-control form-control-lg bsearch-input icon-search-gray-right" 
                                                            placeholder="สถานีบริการที่รับของรางวัล"
                                                            name="GetPTBranch"
                                                            value={this.state.BranchSearchvalue}
                                                            onChange={(e) => this.GetPTBranchSearch(e)}
                                                        />
                                </div>
                            </div>
                        </div>
                   
                        <div className="loader mx-auto" style={{ display: this.state.showLoadingDetail}}></div>
                        <div className="mt-2" style={{ display: (this.state.showLoadingDetail === 'block' ? 'none' : 'block') }}>
                            <div className="row">
                                <div className="col-12">
                                    <div className="list-group search-list"  onChange={(e) => this.handleChange(e)}>
                                        <div style={{ display: (this.state.BranchSearch ? 'none' : 'block') }}>{GetPTBranchCard}</div>
                                        <div style={{ display: (this.state.BranchSearch ? 'block' : 'none') }}>
                                                                <FilterResults
                                                                    value={BranchSearchvalue}
                                                                    data={GetPTBranch}
                                                                    renderResults={results => (
                                                                        <div>
                                                                            {results.map(el => (
                                                                                <div>
                                                                                    <input type="radio" data-address={el.address} name="branchName" value={el.shoP_NAME} id={el.orG_SHOP_ID} />
                                                                                    <label className="list-group-item search-listgitem" for={el.orG_SHOP_ID}>
                                                                                        <div className="t-20 t-greenwhite">{el.shoP_NAME}</div>
                                                                                        <div className="t-18 t-gray t-limit-2 txth-20">{el.address}</div>
                                                                                    </label>
                                                                                </div>
                                                                            ))}
                                                                        </div>
                                                                    )}
                                                                />
                                                            </div>
                                    </div>
                                </div>
                            </div> 
                        </div>  
                        </div>
                        <div className="container fix-bottom py-3">
                            <div className="row">
                                <div className="col-12">
                                    <button onClick={() => this.modalCheckRedeem("ยืนยันใช้คะแนนแลกของรางวัล ?",imgWarning)} disabled={this.state.branchName == "" ? true : false} className="btn btn-block btn-greendark t-22">แลกคะแนน</button>
                                </div>
                            </div>
                        </div>
            </div>
        );
    }
}
