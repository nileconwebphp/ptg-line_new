import React from "react";
import "../../rewards.css";
import FilterResults from 'react-filter-search';
import SweetAlert from "react-bootstrap-sweetalert";
import Rating from "react-rating";
import { promotionActions } from "../../../../_actions";
var crypto = require('crypto');


export class RewardBranchExchange extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            fields: {},
            cardType: {},
            getDetail: {},
            checkTypeEstamp: false,
        }; 
    }

    componentDidMount() {
        const params = this.props.match.params;
        this.setState({
            rewardsId: params.rewardsId
        });
        this.decryptAction(sessionStorage.getItem('reqData'), params.rewardsId);
    }

    decryptAction(subToken, promotionid){
        console.log(subToken)
        var tokenencode = decodeURIComponent(subToken)
        let { requestData } = this.state; 
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
        const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
        var decipher = crypto.createDecipheriv('aes-256-cbc', ENC_KEY, IV);
        var dec = decipher.update(tokenencode,'base64','utf-8');
        dec += decipher.final('utf-8'); 
        var convertData = JSON.parse(dec);
        this.setState({ requestData : convertData })
        this.getCardList(convertData, promotionid)
    }

    getCardList(data, promotionid){
        var tokenId = data.mobileToken;
        var cusId = data.mobileCusID;
        var showLoading = ''
        var content_page = ''
        promotionActions.getCarcard(cusId,tokenId).then(e =>{
            console.log("getCarcard", e)
            if(e.data.isSuccess === true){
                this.setState({ cardType : e.data.data[0] })
                // this.GetCustomerProfile(data, promotionid)
                this.getDetailPromotion(promotionid)
            }else{
                showLoading = 'none'
                content_page = 'block'
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    getDetailPromotion(promotionid){
        promotionActions.getDetailPromotionByID(promotionid).then(e => {
            console.log("getDetailPromotionByID", e)
            var showLoading = ''
            var content_page = ''
            if(e.data.isSuccess === true){
                this.setState({ getDetail : e.data.data })
                if(e.data.data.redeeM_POINT_TYPE == 5){
                    this.setState({ 
                        checkTypeEstamp: true,
                    })
                } else {
                    this.setState({ 
                        checkTypeEstamp: false,
                    })
                }
                showLoading = "none";
                content_page = "block";
            }else{
                showLoading = "block";
                content_page = "none";
            }
            var th = this
            setTimeout(function(){
                th.setState({ 
                    showLoading : showLoading,
                    content_page: content_page
                })
            },1500)
        })
    }


    render() {
        const { getDetail, checkTypeEstamp, cardType, GetCustomerProfile } = this.state
        return (
            <div> 
                <div className="container de-content py-3">
                    <div className="rewards-card">
                        <div className="row">
                            <div className="col-5 pr-0">
                                <img 
                                        src={getDetail.imG_URL_NORMAL == null ? `${process.env.PUBLIC_URL}/images/defaultPT.png` : getDetail.imG_URL_NORMAL}
                                    className="img-fluid w-100"
                                    alt="ptg"
                                />
                                <div className="recards-badge" style={{ display: (getDetail.isNew == true ? 'block' : 'none') }}>ใหม่</div>
                            </div>
                            <div className="col-7 d-flex align-items-start flex-column pt-3 pr-4">
                                    <div className="mb-auto t-green t-limit-3 col1-recards-txt pb-3">{getDetail.redeeM_NAME}</div>
                                <div className="row pt-5">
                                    <div className="col-6 vote">
                                        <Rating
                                            {...this.props} 
                                            initialRating={this.state.rate}
                                            placeholderRating={getDetail.ratingStar == null ? "0" : getDetail.ratingStar}
                                            emptySymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargray.png"} className="col1-recards-rate"/>}
                                            placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargreen.png"} className="col1-recards-rate"/>}
                                            readonly
                                        />
                                        <span className="t-green col1-recards-rate-txt my-auto">({getDetail.ratingStar == null ? "0" : getDetail.ratingStar})</span>
                                    </div>
                                    <div className="col-6 text-right point">
                                        <button type="button" className="btn col1-recards-btn-greenhalf">
                                            <div className="">แลกแต้ม </div>
                                            <div className=""> {checkTypeEstamp ? getDetail.redeeM_STAMP : getDetail.redeeM_TOTAL_POINT} </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div className="row t-20">
                        <div className="col-12">
                            <div className="t-gray pb-1 t-bold">สาขาที่เลือกรับสินค้า</div>
                            <a href="/line/rewards/branch/search">
                                <input 
                                    type="text" 
                                    className="form-control form-control-lg exchange-input t-bold" 
                                    disabled
                                    value={sessionStorage.getItem('branchName')}
                                />
                            </a>
                        </div>
                    </div>
                    <br/>
                    <div className="row px-3">
                        <div className="col-12 p-0 t-20 t-gray t-bold">
                            <div className="">ที่อยู่สาขา</div>
                        </div>
                        <div className="col-12 p-0 pl-1 t-20 t-gray">
                            <div className="txth-20">{sessionStorage.getItem('branchAddress')}</div>
                        </div>
                    </div>
                    <br/>
                    <div className="row px-3">
                        <div className="col-12 p-0 t-20 t-gray t-bold">
                            <div className="">วิธีการใช้งาน</div>
                        </div>
                        <div className="col-12 p-0 pl-1 t-20 t-gray">
                            <div className="txth-20">การใช้แลกรับสินค้า สามารถเลือกรับสินค้าตามสาขาที่เลือก และเมื่อกดแลกสินค้าแล้ว ทางบริษัทจะจัดส่งสินค้าตามสาขาที่ระบุ</div>
                        </div>
                    </div>
                </div>
                {/* <div className="container fix-bottom py-3">
                    <div className="row">
                        <div className="col-12">
                            <a href="/line/rewards/branch" className="btn btn-block btn-greendark t-22">กลับไปหน้าหลัก</a>
                        </div>
                    </div>
                </div> */}
            </div>
        );
    }
}
