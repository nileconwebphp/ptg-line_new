import React from "react";
import "../rewards.css";
import Rating from "react-rating";
import { promotionActions } from "../../../_actions";
import { Base_API } from '../../../_constants/matcher'
import InfiniteLoader from 'react-infinite-loader'
import LazyLoad from "react-lazy-load";
import moment from 'moment-timezone';
import SweetAlert from "react-bootstrap-sweetalert";

var crypto = require('crypto');
var pageID = 1;

export class RewardsEStamp extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            fields: {},
            cate: [],
            listPromotion:[],
            getListByID: [],
            getAvailable : false,
            showLoading: 'block',
            content_page: 'none',
            page:1,
            show: false,
            modal: null,
            isHideLoadmore: false,
            getCheckdataList: '',
            cardType: {},
        }; 
    }

  

    componentDidMount(){
        const params = this.props.match.params;
        var tokenData = this.props.location
        var subToken = tokenData.search.substr(7);
        var tokenencode = decodeURIComponent(subToken)
        
        var unixStart = Math.round((new Date()).getTime() / 1000 );   

        var unixStartMinli = Math.round((new Date()).getTime());   
        var unixCount3 = unixStartMinli + 3000 * 60
        var unixCount1 = unixStart + 1 * 60

        sessionStorage.setItem('reqData',subToken)

        this.setState({ unixCount1 : unixCount1})

        sessionStorage.setItem('timeFirst',unixCount3)

        this.decryptAction(sessionStorage.getItem('reqData'));
    }

    decryptAction(subToken){
        var tokenencode = decodeURIComponent(subToken)
        var showLoading = ''
        var content_page = ''
        let { requestData } = this.state; 
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
        const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
        var decipher = crypto.createDecipheriv('aes-256-cbc', ENC_KEY, IV);
        var dec = decipher.update(tokenencode,'base64','utf-8');
        dec += decipher.final('utf-8'); 
        var convertData = JSON.parse(dec); 

        this.getCardList(convertData);
        this.setState({ requestData : convertData })
    }

    getCardList(data){
        var userId = "XfBI9ZXkjlw%3D";
        var showLoading = ''
        var content_page = ''
        promotionActions.getCardList(userId).then(e =>{
            console.log("getCardList", e)
            if(e.data.isSuccess === true){
                showLoading = 'none'
                content_page = 'block'
                this.setState({ cardType : e.data.data[0] })
                this.getPromotion(e.data.data[0].carD_TYPE_ID)
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    getPromotion(type){
        var showLoading = ''
        var content_page = ''
        var pageNo = this.state.page
        var textsearch = ''
        var catecode = ''
        console.log("cardTypeId",type)
        promotionActions.GetPromotion_EStamp(type, catecode, pageNo).then(e => {
            console.log("GetPromotion_EStamp", e)
            if(e.data.isSuccess === true){
                showLoading = "none";
                content_page = "block";
                var resp = e.data; 
                var dataList = this.state.listPromotion.concat(resp.data);
                this.setState({ page: Number(this.state.page) + 1 })
                this.setState({ listPromotion : dataList })
                
            }else{
                this.setState({ isHideLoadmore: true })
                if(e.data.errCode === 10){
                    showLoading = "none";
                    content_page = "block";
                    this.setState({ isHideLoadmore: true })
                }else{
                    return false
                }
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    handleLoadItems(getAvailable,code,cardTypeId) {
        if(this.state.isHideLoadmore === false){
            if(getAvailable === false){
                this.getPromotion(cardTypeId);
                console.log("getPromotion")
            }else{
                this.getPromotionbyID(code,false,cardTypeId);
                console.log("getPromotionbyID")
            }
        }else{
            return false;
        }
    }

    nextPageDetail(promotionid){
        sessionStorage.setItem('pr_id' , promotionid)
        // var unixClick = Math.round((new Date()).getTime()); 
        var unixClick = Math.round((new Date()).getTime() / 1000); 

        // if(this.state.unixCount1 > unixClick){
            window.location.href = `/line/rewards/detail1/${promotionid}`
        // }else{
        //     var msg = "เกิดข้อผิดพลาด"
        //     var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_danger2.jpg`;
        //     this.modalCheckSubmit(msg,imgPopup)
            
        // }
    }

    modalCheckSubmit(res,img){
        alert = (
          <SweetAlert
            custom
            confirmBtnBsStyle="success"
            closeOnClickOutside={true}
            cancelBtnBsStyle="default"
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
            onOutsideClick={() => {
              this.wasOutsideClick = true;
              this.setState({ showConfirm: false })
            }}
          >
            <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
            <div className="res-popup">{res}</div>
          </SweetAlert>
        );
    
        this.setState({ show: true , modal: alert })
      }

    
      handleChoice(choice) {
        if (choice === false) {
            this.setState({ show: false , modal: null })
        }
      }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/defaultPT.png`);
      }

    render() {
        const { cardType } = this.state
        var thisList = this.state.listPromotion
        var listFirstPromotion = thisList.map(( item , i) => {
            if(item.redeeM_CATEGORY_ID !== "39816974-7cdf-e911-9458-005056895719"){
                return (
                    <div key={i}>
                        <div onClick={() => this.nextPageDetail(item.promotionId)}>
                            <div className="rewards-card">
                                <div className="row">
                                    <div className="col-5 pr-0">
                                        <div className="LazyLoad is-visible">
                                            <LazyLoad>
                                            <img 
                                                src={item.imG_URL_NORMAL}
                                                className="img-fluid w-100"
                                                alt="ptg"
                                                onError={e => this.onImageError(e)}
                                            />
                                            </LazyLoad>
                                        </div>
                                    </div>
                                    <div className="col-7 d-flex align-items-start flex-column pt-3 pr-4">
                                        <div className="mb-auto t-green t-limit-3 recards-txt">{item.redeeM_NAME}</div>
                                        <div className="row pt-5">
                                        <div className="col-6 vote">
                                            <Rating
                                                {...this.props} 
                                                initialRating={this.state.rate}
                                                placeholderRating={item.ratingStar}
                                                emptySymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargray.png"} className="recards-rate"/>}
                                                placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargreen.png"} className="recards-rate"/>}
                                                readonly
                                            />
                                            <span className="t-green recards-rate-txt my-auto">(0)</span>
                                        </div>
                                        <div className="col-6 text-right point">
                                            <button type="button" className="btn recards-btn-greenhalf txth-15">
                                                <div className="txth-15">แลกแต้ม </div>
                                                <div className="txth-15"> {item.redeeM_STAMP}</div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                </div>
                )
            }else{
                return (<div></div>)
            }
        })
        return (
            <div>
                <div id="loading" style={{display : this.state.showLoading}}>
                    <div className="content_class">
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                </div>
                <div className="container content_page" style={{ display: this.state.content_page}}>
                    <div className="tab-content">
                        <div className="tab-pane active">
                            <div className="form-group"></div>
                            <div className="row">
                                <div className="col-md-12 t-20 mb-2">
                                    คุณมีคะแนนคงเหลือ <span className="t-green"> {parseInt(cardType.carD_ESTAMP)} ดวง </span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    {listFirstPromotion}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style={this.state.isHideLoadmore === true ? {display:'none'} : {display:'block'}}>
                    <InfiniteLoader onVisited={ () => this.handleLoadItems(this.state.getAvailable,this.state.code_cate,this.state.cardType.carD_TYPE_ID)}/>
                </div>
            {this.state.modal}
        </div>
        );
    }
}
