import React from "react";
import "./register.css";
import Rating from "react-rating";

export class Register extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            fields: {},
        }; 
    }

    componentDidMount() {
       
    }

    render() {
        return (
            <div> 

                <div className="container">
                    <div className="regis-content my-3">
                        <div className="row text-center py-3">
                            <div className="col-12">
                                <div className="t-bold t-30 t-green">Patient Register</div>
                            </div>
                        </div>
                        <div className="row p-3">
                            <div className="col-12">
                                <div className="form-group">
                                    <input type="text" className="form-control regis-input" placeholder="ชื่อ *"/>
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control regis-input" placeholder="นามสกุล *"/>
                                </div>
                                <div className="form-group">
                                    <label>วันเดือนปีเกิด * </label>
                                    <input type="date" className="form-control regis-input"/>
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control regis-input" placeholder="เบอร์มือถือ *"/>
                                </div>
                                <div className="form-group">
                                    <input type="text" className="form-control regis-input" placeholder="อีเมล *"/>
                                </div>
                                <button type="button" className="btn regis-btn">Submit</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        );
    }
}
