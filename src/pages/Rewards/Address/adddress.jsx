import React from "react";
import "../rewards.css";
import Rating from "react-rating";
import SweetAlert from "react-bootstrap-sweetalert";
import { promotionActions } from "../../../_actions";
var crypto = require('crypto');
var imgWarning = `${process.env.PUBLIC_URL}/images/info.png`;

export class RewardAdress extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            fields: {},
            shows: false,
            getDetail: {},
            cardType: {},
            GetCustomerProfile: {},
            checkTypeEstamp: false,
            address: "",
            errors: {
                address: "",
            },
            errorsFocus: {
                address: "",
            },
        }; 
    }

    componentDidMount() {
        const params = this.props.match.params;
        this.setState({
            rewardsId: params.rewardsId
        });
        this.getDetailPromotion(params.rewardsId)
        this.decryptAction(sessionStorage.getItem('reqData'), params.rewardsId);
    }

    decryptAction(subToken, promotionid){
        console.log(subToken)
        var tokenencode = decodeURIComponent(subToken)
        let { requestData } = this.state; 
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
        const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
        var decipher = crypto.createDecipheriv('aes-256-cbc', ENC_KEY, IV);
        var dec = decipher.update(tokenencode,'base64','utf-8');
        dec += decipher.final('utf-8'); 
        var convertData = JSON.parse(dec);
        this.setState({ requestData : convertData })
        this.getCardList(convertData, promotionid)
    }

    getCardList(data, promotionid){
        var tokenId = data.mobileToken;
        var cusId = data.mobileCusID;
        var showLoading = ''
        var content_page = ''
        promotionActions.getCarcard(cusId,tokenId).then(e =>{
            console.log("getCarcard", e)
            if(e.data.isSuccess === true){
                this.setState({ cardType : e.data.data[0] })
                // this.GetCustomerProfile(data, promotionid)
                this.getDetailPromotion(promotionid)
            }else{
                showLoading = 'none'
                content_page = 'block'
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    getDetailPromotion(promotionid){
        promotionActions.getDetailPromotionByID(promotionid).then(e => {
            console.log("getDetailPromotionByID", e)
            var showLoading = ''
            var content_page = ''
            if(e.data.isSuccess === true){
                this.setState({ getDetail : e.data.data })
                if(e.data.data.redeeM_POINT_TYPE == 5){
                    this.setState({ 
                        checkTypeEstamp: true,
                    })
                } else {
                    this.setState({ 
                        checkTypeEstamp: false,
                    })
                }
                showLoading = "none";
                content_page = "block";
            }else{
                showLoading = "block";
                content_page = "none";
            }
            var th = this
            setTimeout(function(){
                th.setState({ 
                    showLoading : showLoading,
                    content_page: content_page
                })
            },1500)
        })
    }

    onSubmit(){
        let errors = {};
        let errorsFocus = {};
        const { address } = this.state
        if(address === ""){
            errorsFocus["address"] = 'errorFocus'
            errors["address"] = "โปรดระบุที่อยู่ในการจัดส่ง"
            this.setState({ errorsFocus, errors })
        } else {
            this.modalCheckRedeem("ยืนยันใช้คะแนนแลกของรางวัล ?",imgWarning)
        }
    }

    modalCheckRedeem(res, img) {
        alert = (
            <SweetAlert
            custom
            showCancel
            showCloseButton
            confirmBtnText={"ยืนยัน"}
            cancelBtnText={"ยกเลิก"}
            confirmBtnBsStyle="success"
            cancelBtnBsStyle="light"
            // closeOnClickOutside={false}
            // focusConfirmBtn={false}
            // title=""
            customIcon={img}
            showConfirm={true}
            showCancel={true}
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
            reverseButtons={true}
          >
            <div className="iconClose" onClick={() => this.handleChoice(false)}></div>
            <div className="fontSizeCase">{res}</div>
          </SweetAlert>
        );
        this.setState({ show: true, modal: alert });
    }

    InsertRedeemPromotion(){
        const { requestData, address } = this.state
        var showLoading = '';
        var content_page = '';
        var dataRedeem = {
            TOKEN_ID: requestData.mobileToken,
            CUSTOMER_ID: requestData.mobileCusID,
            CARD_MASTER_ID: requestData.mobileCardMasterID,
            SHIPPING_TYPE: '2',
            SHIPPING_SHOP_ID: '',
            REDEEM_CODE: sessionStorage.getItem('redeem_code'),
            FullAddress:  address,
            REDEEM_DETAIL:'',
            SOURCE_DATA: 19
        }
        promotionActions.insertRedeem(dataRedeem).then( e => {
            if(e.data.isSuccess === true){
                showLoading = 'none';
                content_page = 'block';
                var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_correct1@3x.png`;
                this.modalCheckSubmitSuccess("แลกของรางวัลสำเร็จ",imgPopup)
                sessionStorage.setItem('address',address)
            }else{
                this.setState({ msg : e.data.errMsg })
                var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_danger2.jpg`;
                this.modalCheckSubmit(e.data.errMsg,imgPopup)
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },1000)
        })
    }

    onConfirmtoMain () {
        this.setState({ shows: false })
        window.location.href="/line/rewards"
    }


    modalCheckSubmitSuccess(res,img){
        alert = (
          <SweetAlert
            custom
            confirmBtnBsStyle="success"
            closeOnClickOutside={true}
            cancelBtnBsStyle="default"
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoiceSuccess(false)}
            onConfirm={() => this.handleChoiceSuccess(true)}
            onOutsideClick={() => {
              this.wasOutsideClick = true;
              this.setState({ showConfirm: false })
            }}
          >
            <div className="iconClose" onClick={() => this.handleChoiceSuccess(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
            <div className="res-popup">{res}</div>
          </SweetAlert>
        );
    
        this.setState({ show: true , modal: alert })
      } 

      handleChoiceSuccess(choice) {
          const { rewardsId } = this.state
        if (choice === false) {
            this.setState({ show: false , modal: null })
            window.location.href = `/line/rewards/address/success/${this.state.rewardsId}`
        }
      }


    modalCheckSubmit(res,img){
        alert = (
          <SweetAlert
            custom
            confirmBtnBsStyle="success"
            closeOnClickOutside={true}
            cancelBtnBsStyle="default"
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
            onOutsideClick={() => {
              this.wasOutsideClick = true;
              this.setState({ showConfirm: false })
            }}
          >
            <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
            <div className="res-popup">{res}</div>
          </SweetAlert>
        );
    
        this.setState({ show: true , modal: alert })
      }

    
      handleChoice(choice) {
        if (choice === false) {
            this.setState({ show: false , modal: null })
        }else{
            this.InsertRedeemPromotion()
        }
      }


    handleChange = (e) => {
        let { errors , errorsFocus } = this.state;
        const input = e.target;
        const value = input.value;
        errors["address"] = null;
        errorsFocus["address"] = ''
        this.setState({ address: value, errorsFocus, errors });
    };

    render() {
        const { getDetail, checkTypeEstamp, cardType, GetCustomerProfile } = this.state
        return (
            <div> 
                {this.state.modal}
                <SweetAlert 
                    show={this.state.shows}
                    confirmBtnCssClass="btn-block btn-lg btn-sweetalert-green"
                    confirmBtnText="ตกลง"
                    title={<span className="text-26">การแจ้งเตือน</span>} 
                    onConfirm={() => this.onConfirmtoMain()}
                >
                    <span>
                        <img 
                            src={process.env.PUBLIC_URL +"/images/icon/icon_correct1@3x.png"} 
                            className="py-3" 
                            alt="sena"
                            width="70"
                        />
                        <br></br>
                        <span className="t-24">แลกรางวัลเรียบร้อยแล้ว</span>
                    </span>
                </SweetAlert>
                <div className="container de-content2 py-3">
                    {/* <div className="row">
                        <div className="col-12">
                            <div className="t-18 t-bold t-gray">
                                คะแนนคงเหลือของคุณ : 
                                <span className="t-greenwhite">
                                    {checkTypeEstamp ?
                                    " " + parseInt(cardType.carD_ESTAMP) + " ดวง"
                                    :
                                    " " + parseInt(cardType.carD_POINT) + " คะแนน"
                                    }
                                </span>
                            </div>
                        </div>
                    </div>
                    <hr className="address-hrtop"/> */}
                    <div className="row t-18 t-bold">
                        <div className="col-12">
                            <div className="t-gray pb-1">ที่อยู่ในการจัดส่งของคุณ</div>
                            <div className="card address-card">
                                <input 
                                    type="text" 
                                    className={`form-control form-control-lg address-input ${this.state.errorsFocus['address']}`}
                                    value={this.state.address}
                                    onChange={(e) => this.handleChange(e)}
                                    placeholder="ระบุที่อยู่ในการจัดส่ง"
                                />
                            </div>
                            <div className="errorMsg address">{this.state.errors["address"]}</div>
                        </div>
                    </div>
                    <hr/>
                    <div className="rewards-card mb-3">
                        <div className="row">
                            <div className="col-5 pr-0">
                                <img 
                                    src={getDetail.imG_URL_NORMAL == null ? `${process.env.PUBLIC_URL}/images/defaultPT.png` : getDetail.imG_URL_NORMAL}
                                    className="img-fluid w-100"
                                    alt="ptg"
                                />
                                <div className="recards-badge" style={{ display: (getDetail.isNew == true ? 'block' : 'none') }}>ใหม่</div>
                            </div>
                            <div className="col-7 d-flex align-items-start flex-column pt-3 pr-4">
                                <div className="mb-auto t-green t-limit-3 col1-recards-txt pb-3">{getDetail.redeeM_NAME}</div>
                                <div className="row pt-5">
                                    <div className="col-6 vote">
                                        <Rating
                                            {...this.props} 
                                            initialRating={this.state.rate}
                                            placeholderRating={getDetail.ratingStar == null ? "0" : getDetail.ratingStar}
                                            emptySymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargray.png"} className="col1-recards-rate"/>}
                                            placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargreen.png"} className="col1-recards-rate"/>}
                                            readonly
                                        />
                                            <span className="t-green col1-recards-rate-txt my-auto">({getDetail.ratingStar == null ? "0" : getDetail.ratingStar})</span>
                                    </div>
                                    <div className="col-6 text-right point">
                                        <button type="button" className="btn col1-recards-btn-greenhalf">
                                            <div className="">แลกแต้ม </div>
                                            <div className=""> {checkTypeEstamp ? getDetail.redeeM_STAMP : getDetail.redeeM_TOTAL_POINT} </div>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12 p-0 pl-3 t-20 t-gray t-bold">
                            <div className="">วิธีการใช้งาน</div>
                        </div>
                        <div className="col-12 p-0 pl-3 t-20 t-gray">
                            <div className="txth-20">การใช้แลกรับสินค้า สามารถเลือกรับสินค้าตามสาขาที่เลือก และเมื่อกดแลกสินค้าแล้ว ทางบริษัทจะจัดส่งสินค้าตามสาขาที่ระบุ</div>
                        </div>
                    </div>
                </div>
                <div className="container fix-bottom py-3">
                    <hr/>
                    <div className="row pb-3 t-gray t-bold  t-20">
                        <div className="col-6">
                            คะแนนที่ใช้แลกทั้งหมด
                        </div>
                        <div className="col-6 text-right t-bold t-greenwhite">
                            {checkTypeEstamp ? getDetail.redeeM_STAMP : getDetail.redeeM_TOTAL_POINT} คะแนน
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <a 
                                className="btn btn-block btn-greendark t-22"
                                onClick={() => this.onSubmit()} 
                            > 
                                ยืนยัน
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
