let config;
const urlPath = window.location.hostname
if(urlPath === 'localhost'){
  config = {
    promotion : 'https://asv-mobileapp-dev.azurewebsites.net/api/Promotion',
    webview : 'https://asv-mobileapp-dev.azurewebsites.net/api/webview', 
    apis : 'https://asv-mobileapp-dev.azurewebsites.net/api',
    // linePush : 'https://asv-pt-maxcard-dev.azurewebsites.net/api'
    linePush : 'https://asv-mobile-pt-maxcard-dev.azurewebsites.net/api'
  }
}else{
  // config = {
  //   promotion : 'https://asv-mobileapp-dev.azurewebsites.net/api/Promotion',
  //   webview : 'https://asv-mobileapp-dev.azurewebsites.net/api/webview', 
  //   apis : 'https://asv-mobileapp-dev.azurewebsites.net/api',
  //   // linePush : 'https://asv-pt-maxcard-dev.azurewebsites.net/api'
  //   linePush : 'https://asv-mobile-pt-maxcard-dev.azurewebsites.net/api'
  // }
  config = {
    promotion : 'https://asv-mobileapp-prod.azurewebsites.net/api/Promotion',
    webview : 'https://asv-mobileapp-prod.azurewebsites.net/api/webview', 
    apis : 'https://asv-mobileapp-prod.azurewebsites.net/api',
    // linePush : 'https://asv-pt-maxcard-dev.azurewebsites.net/api'
    linePush : 'https://asv-mobile-pt-maxcard-prod.azurewebsites.net/api'
  }
}


export const Base_API = config;
