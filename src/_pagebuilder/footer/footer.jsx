import React, { Component } from "react";

export class Footer extends Component {
  render() {
    return (
      <div className="footer text-center text-muted">
        &copy; 2015. Workpoint
      </div>
    );
  }
}
