import React from "react";
import "../rewards.css";
import Rating from "react-rating";
import { promotionActions } from "../../../_actions";
import { Base_API } from '../../../_constants/matcher'
import InfiniteLoader from 'react-infinite-loader'
import LazyLoad from "react-lazy-load";
import moment from 'moment-timezone';
import SweetAlert from "react-bootstrap-sweetalert";

var crypto = require('crypto');
var pageID = 1;

export class Rewards extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            fields: {},
            cate: [],
            listPromotion:[],
            listPromotionEStamp: [],
            getListByID: [],
            getAvailable : false,
            showLoading: 'block',
            content_page: 'none',
            page:1,
            show: false,
            modal: null,
            isHideLoadmore: false,
            getCheckdataList: '',
            pageCheckType: 'promotion',
            pageNoestamp: 1
        }; 
    }

  

    componentDidMount(){
        const params = this.props.match.params;
        var tokenData = this.props.location
        var subToken = tokenData.search.substr(7);
        var tokenencode = decodeURIComponent(subToken)
        
        var unixStart = Math.round((new Date()).getTime() / 1000 );   

        var unixStartMinli = Math.round((new Date()).getTime());   
        var unixCount3 = unixStartMinli + 3000 * 60
        var unixCount1 = unixStart + 1 * 60

        sessionStorage.setItem('reqData',subToken)

        this.setState({ unixCount1 : unixCount1})

        sessionStorage.setItem('timeFirst',unixCount3)

        this.decryptAction(sessionStorage.getItem('reqData'));

        // var data = `{
        //     "lineID" : "LINETEST000000XXXX",
        //     "mobileToken": "d0868091-99cb-493e-86ec-da753954b70c",
        //     "mobileCusID": "a4d4a544-ce6c-4cd4-be79-679d2a766f23",
        //     "mobileCardNo": "1201313537",
        //     "mobileCardMasterID": "21e37e45-e8ef-e911-9440-00505689760f",
        //     "mobileCardTypeID": "3fe3b1d4-da87-e311-9406-00505689235e",
        //     "unixTime": 1644456054
        // }`
        // const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019";
        // const IV = "xRtyUw5Pt+58ZxqA";
        // let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
        // let encrypted = cipher.update(data);
        // encrypted = Buffer.concat([encrypted, cipher.final()]);
        // var setParam = encrypted.toString('base64');
        // console.log("setParam",setParam)
    }


    decryptAction(subToken){
        var tokenencode = decodeURIComponent(subToken)
        var showLoading = ''
        var content_page = ''
        let { requestData } = this.state; 
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
        const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
        var decipher = crypto.createDecipheriv('aes-256-cbc', ENC_KEY, IV);
        var dec = decipher.update(tokenencode,'base64','utf-8');
        dec += decipher.final('utf-8'); 
        var convertData = JSON.parse(dec); 
        this.getCardList(convertData);
        this.getCategoryList()
        this.setState({ requestData : convertData })
    }

    getCardList(data){
        var tokenId = data.mobileToken;
        var cusId = data.mobileCusID;
        var showLoading = ''
        var content_page = ''
        promotionActions.getCarcard(cusId,tokenId).then(e =>{
            console.log("getCarcard", e)
            if(e.data.isSuccess === true){
                showLoading = 'none'
                content_page = 'block'
                this.setState({ cardType : e.data.data[0] })
                this.getPromotion(e.data.data[0].carD_TYPE_ID)
            }else{
                showLoading = 'none'
                content_page = 'block'
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    getCategoryList(){
        promotionActions.getCateList().then(e => {
            if(e.isSuccess === true){ 
                this.setState({ cate : e.data }) 
                this.setState({ cateCheckSearch : e.data.redeeM_CATEGORY_ID })
            }else{

            }
        })
    }

    handleChange(e){
        var txtSearch = e.target.value        
    }

    getPromotionbyID(cateCode,res,cardType){
        if(res === true){
            this.setState({ getListByID : []})
            pageID = 1
        }else{
            pageID += 1;
        }
        let { code_cate , getAvailable } = this.state;
        code_cate = cateCode
        getAvailable = true
        this.setState({ code_cate , getAvailable })
        
        var textsearch = ''
        var showLoading = ''
        var content_page = ''
        promotionActions.getPromotionAll(pageID,textsearch,cateCode,cardType).then(e => {
            if(e.data.isSuccess === true){
                showLoading = 'none';
                content_page = 'block';
                var response = e.data; 
                var dataListID = this.state.getListByID.concat(response.data);
                // this.setState({ pageId: Number(this.state.pageID) + 1 })
                this.setState({ getListByID : dataListID })
            }else{
                this.setState({ isHideLoadmore: true })
                showLoading = "none";
                content_page = "block";
            }
            var th = this;
            setTimeout(function(){
                th.setState({ showLoading : showLoading })
                th.setState({ content_page : content_page })
            },2000)
        })
    }

    pushList(res,response){
        if(res === true){
            var dataListID = this.state.getListByID.concat(response.data);
            this.setState({ page: Number(this.state.page) + 1 })
            this.setState({ getListByID : dataListID })
        }
    }

    getPromotion(type){
        var showLoading = ''
        var content_page = ''
        this.state.getAvailable = false
        var pageNo = this.state.page
        var textsearch = ''
        var catecode = ''
        console.log("pageNo",pageNo)
        promotionActions.getPromotionAll(pageNo,textsearch,catecode,type).then(e => {
            console.log("getPromotionAll",e)
            if(e.data.isSuccess === true){
                showLoading = "none";
                content_page = "block";
                var resp = e.data; 
                var dataList = this.state.listPromotion.concat(resp.data);
                this.setState({ page: Number(this.state.page) + 1 })
                this.setState({ listPromotion : dataList, pageCheckType: 'promotion' })
            }else{
                this.setState({ isHideLoadmore: true })
                if(e.data.errCode === 10){
                    showLoading = "none";
                    content_page = "block";
                    this.setState({ isHideLoadmore: true })
                }else{
                    return false
                }
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    getPromotionEStamp(){
        this.setState({ pageCheckType: 'estamp' })
        const { cardType } = this.state
        var showLoading = ''
        var content_page = ''
        var pageNoestamp = this.state.pageNoestamp
        var textsearch = ''
        var catecode = ''
        var cardTypeId = cardType.carD_TYPE_ID
        console.log("cardTypeId",cardTypeId)
        console.log("pageNoestamp",pageNoestamp)
        promotionActions.GetPromotion_EStamp(cardTypeId, catecode, pageNoestamp).then(e => {
            console.log("GetPromotion_EStamp", e)
            if(e.data.isSuccess === true){
                showLoading = "none";
                content_page = "block";
                var resp = e.data; 
                var dataList = this.state.listPromotionEStamp.concat(resp.data);
                this.setState({ pageNoestamp: Number(this.state.pageNoestamp) + 1 })
                this.setState({ listPromotionEStamp : dataList })
            }else{
                this.setState({ isHideLoadmore: true })
                if(e.data.errCode === 10){
                    showLoading = "none";
                    content_page = "block";
                    this.setState({ isHideLoadmore: true })
                }else{
                    return false
                }
            }
            var th = this
            setTimeout(function(){
                th.setState({ showLoading : showLoading})
                th.setState({ content_page: content_page })
            },2000)
        })
    }

    handleLoadItems(getAvailable,code,cardTypeId) {
        if(this.state.isHideLoadmore === false){
            if(getAvailable === false){
                if(this.state.pageCheckType === "estamp"){
                    this.getPromotionEStamp();
                } else {
                    this.getPromotion(cardTypeId);
                }
            }else{
                this.getPromotionbyID(code,false,cardTypeId);
            }
        }else{
            return false;
        }
    }

    nextPageDetail(promotionid){
        sessionStorage.setItem('pr_id' , promotionid)
        // var unixClick = Math.round((new Date()).getTime()); 
        var unixClick = Math.round((new Date()).getTime() / 1000); 
        console.log(this.state.unixCount1);
        console.log(unixClick);
        if(this.state.unixCount1 > unixClick){
            window.location.href = `/line/rewards/detail1/${promotionid}`
        }else{
            var msg = "เกิดข้อผิดพลาด"
            var imgPopup = `${process.env.PUBLIC_URL}/images/icon/icon_danger2.jpg`;
            this.modalCheckSubmit(msg,imgPopup)
            
        }

        
    }

    modalCheckSubmit(res,img){
        alert = (
          <SweetAlert
            custom
            confirmBtnBsStyle="success"
            closeOnClickOutside={true}
            cancelBtnBsStyle="default"
            focusConfirmBtn={false}
            title=""
            customIcon={img}
            showConfirm={false}
            showCancelButton
            onCancel={() => this.handleChoice(false)}
            onConfirm={() => this.handleChoice(true)}
            onOutsideClick={() => {
              this.wasOutsideClick = true;
              this.setState({ showConfirm: false })
            }}
          >
            <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
            <div className="res-popup">{res}</div>
          </SweetAlert>
        );
    
        this.setState({ show: true , modal: alert })
      }

    
      handleChoice(choice) {
        if (choice === false) {
            this.setState({ show: false , modal: null })
        }
      }

    onImageError(e) {
        e.target.setAttribute("src", `${process.env.PUBLIC_URL}/images/defaultPT.png`);
      }

    render() {
        if(this.state.cateCheckSearch === null || this.state.cateCheckSearch === '' || this.state.cateCheckSearch === undefined){
            this.state.cateCheckSearch = ''
        }
        var thisCate = this.state.cate;
        var letList = thisCate.map(( item,i ) => {
            return (
                <li key={i} className="li-overflow pr-2" style={(item.cataloG_CODE === "RC2019092501" || item.cataloG_CODE === 'RC2019081601' ? {display:'none'} : {display:'block'})}>
                    <a className="nav-link a-overflow" onClick={() => this.getPromotionbyID(item.redeeM_CATEGORY_ID,true,this.state.cardType.carD_TYPE_ID)} data-toggle="pill" href=''>{item.cataloG_NAME}</a>
                </li>
            )
        })

        const { listPromotionEStamp, pageCheckType } = this.state
        var listPromotionEStampCard = listPromotionEStamp.map(( item , i) => {
            // if(item.redeeM_CATEGORY_ID !== "39816974-7cdf-e911-9458-005056895719"){
                return (
                    <div key={i}>
                        <div onClick={() => this.nextPageDetail(item.promotionId)}>
                            <div className="rewards-card">
                                <div className="row">
                                    <div className="col-5 pr-0">
                                        <div className="LazyLoad is-visible">
                                            <LazyLoad>
                                            <img 
                                                src={item.imG_URL_NORMAL}
                                                className="img-fluid w-100"
                                                alt="ptg"
                                                onError={e => this.onImageError(e)}
                                            />
                                            </LazyLoad>
                                        </div>
                                    </div>
                                    <div className="col-7 d-flex align-items-start flex-column pt-3 pr-4">
                                        <div className="mb-auto t-green t-limit-3 recards-txt pb-2">{item.redeeM_NAME}</div>
                                        <div className="row pt-5">
                                        <div className="col-6 vote">
                                            <Rating
                                                {...this.props} 
                                                initialRating={this.state.rate}
                                                placeholderRating={item.ratingStar}
                                                emptySymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargray.png"} className="recards-rate"/>}
                                                placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargreen.png"} className="recards-rate"/>}
                                                readonly
                                            />
                                            <span className="t-green recards-rate-txt my-auto">(0)</span>
                                        </div>
                                        <div className="col-6 text-right point">
                                            <button type="button" className="btn recards-btn-greenhalf txth-15">
                                                <div className="txth-15">แลกแต้ม </div>
                                                <div className="txth-15"> {item.redeeM_STAMP}</div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                </div>
                )
            // }else{
            //     return (<div></div>)
            // }
        })

        var thisList = this.state.listPromotion
        var listFirstPromotion = thisList.map(( item , i) => {
            // if(item.redeeM_CATEGORY_ID !== "39816974-7cdf-e911-9458-005056895719"){
                return (
                    <div key={i}>
                        <div onClick={() => this.nextPageDetail(item.promotionId)}>
                            <div className="rewards-card">
                                <div className="row">
                                    <div className="col-5 pr-0">
                                        <div className="LazyLoad is-visible">
                                            <LazyLoad>
                                            <img 
                                                src={item.imG_URL_NORMAL}
                                                className="img-fluid w-100"
                                                alt="ptg"
                                                onError={e => this.onImageError(e)}
                                            />
                                            </LazyLoad>
                                        </div>
                                    </div>
                                    <div className="col-7 d-flex align-items-start flex-column pt-3 pr-4">
                                        <div className="mb-auto t-green t-limit-3 recards-txt pb-2">{item.redeeM_NAME}</div>
                                        <div className="row pt-5">
                                        <div className="col-6 vote">
                                            <Rating
                                                {...this.props} 
                                                initialRating={this.state.rate}
                                                placeholderRating={item.ratingStar}
                                                emptySymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargray.png"} className="recards-rate"/>}
                                                placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargreen.png"} className="recards-rate"/>}
                                                readonly
                                            />
                                            <span className="t-green recards-rate-txt my-auto">(0)</span>
                                        </div>
                                        <div className="col-6 text-right point">
                                            <button type="button" className="btn recards-btn-greenhalf txth-15">
                                                <div className="txth-15">แลกแต้ม </div>
                                                <div className="txth-15"> {item.redeeM_TOTAL_POINT}</div>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr/>
                </div>
                )
            // }else{
            //     return (<div></div>)
            // }
        })

        var thisListId = this.state.getListByID;
        var getPromotionByRedeem = thisListId.map(( item , i ) => {
            if(item.redeeM_CATEGORY_ID !== "39816974-7cdf-e911-9458-005056895719"){
                return (
                    <div>
                        <div onClick={() => this.nextPageDetail(item.promotionId)}>
                            <div className="rewards-card">
                                <div className="row">
                                    <div className="col-5 pr-0">
                                        <div className="LazyLoad is-visible">
                                        <LazyLoad>
                                            <img 
                                                src={item.imG_URL_NORMAL}
                                                className="img-fluid w-100"
                                                alt="ptg"
                                                onError={e => this.onImageError(e)}
                                            />
                                        </LazyLoad>
                                        </div>
                                    </div>
                                    <div className="col-7 d-flex align-items-start flex-column pt-3 pr-4  pb-2">
                                        <div className="mb-auto t-green t-limit-3 recards-txt">{item.redeeM_NAME}</div>
                                        <div className="row pt-5">
                                        <div className="col-6 vote">
                                        <Rating
                                            {...this.props} 
                                            initialRating={this.state.rate}
                                            placeholderRating={item.ratingStar}
                                            emptySymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargray.png"} className="recards-rate"/>}
                                            placeholderSymbol={<img src={process.env.PUBLIC_URL +"/images/icon/stargreen.png"} className="recards-rate"/>}
                                            readonly
                                        />
                                        <span className="t-green recards-rate-txt my-auto">(0)</span>
                                    </div>
                                    <div className="col-6 text-right point">
                                        <button type="button" className="btn recards-btn-greenhalf txth-15">
                                            <div className="txth-15">แลกแต้ม </div>
                                            <div className="txth-15"> {item.redeeM_TOTAL_POINT}</div>
                                        </button>
                                    </div>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                    </div>
                )
            }else{
                return (<div></div>)
            }
        })
        return (
            <div>
                <div id="loading" style={{display : this.state.showLoading}}>
                    <div className="content_class">
                        <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                    </div>
                </div>
                <div className="container content_page" style={{ display: this.state.content_page}}>
                    <div className="fix-top">
                    <ul className="nav nav-pills np-overflow2 d-flex flex-nowrap text-center">
                        <li className="li-overflow2 pr-2">
                            <a className={`nav-link a-overflow2 ${pageCheckType === "promotion" ? "active" : ""}`} onClick={() => this.getPromotion(this.state.cardType.carD_TYPE_ID)}>สิทธิพิเศษและของรางวัล</a>
                        </li>
                        <li className="li-overflow2">
                            <a className={`nav-link a-overflow2 ${pageCheckType === "promotion" ? "" : "active"}`} onClick={() => this.getPromotionEStamp()}>ของรางวัล E-Stamp</a>
                        </li>
                    </ul>
                    {pageCheckType === "promotion" ?
                    <ul className="nav nav-pills np-overflow d-flex flex-nowrap text-center">
                        <li className="li-overflow pr-2">
                            <a className="nav-link active a-overflow" data-toggle="pill" href="#all" onClick={() => this.getPromotion(this.state.cardType.carD_TYPE_ID)}>ทั้งหมด</a>
                        </li>
                        {letList}
                    </ul>
                    :
                    null
                    }
                    </div>
                <div className={`${pageCheckType === "promotion" ? "pt-5" : "" } content`}>
                    <div className="tab-content">
                        <div className="tab-pane active">
                            <div className="form-group">
                                {/* <input 
                                    type="text" 
                                    className="form-control icon-search-gray input-search" 
                                    placeholder="Search..."
                                    name="searching" 
                                    onChange={this.handleChange}
                                />
                                <button className="click-search" onClick={() => this.clickCheckSearch(this.state.cateCheckSearch)}><i className="fas fa-share"></i></button> */}
                            </div>
                            {pageCheckType === "promotion" ?
                            <div className="row">
                                <div className="col-md-6">
                                    {(this.state.getAvailable === true ? getPromotionByRedeem : listFirstPromotion)}
                                </div>
                            </div>
                            :
                            <div className="row">
                                <div className="col-md-6">
                                    {listPromotionEStampCard}
                                </div>
                            </div>
                            }
                        </div>
                    </div>
                </div>
                <div style={this.state.isHideLoadmore === true ? {display:'none'} : {display:'block'}}>
                    <InfiniteLoader onVisited={ () => this.handleLoadItems(this.state.getAvailable,this.state.code_cate,this.state.cardType.carD_TYPE_ID)}/>
                </div>
            </div>
            {this.state.modal}
        </div>
        );
    }
}
