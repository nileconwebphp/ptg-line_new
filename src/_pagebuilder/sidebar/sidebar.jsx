import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "./sidebar.css";

export class Sidebar extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleActive = this.handleActive.bind(this);

    this.state = {
      active: '',
      subactive: ''
    };
  }

  handleActive(menuItem) {
    this.setState({ active: '' });
    this.setState({ subactive: '' });
    this.setState({ active: menuItem });
  }

  handleActiveSubmenu(menuItem) {
    this.setState({ subactive: '' });
    this.setState({ subactive: menuItem });
  }

  render() {
    const activeStyle = 'active'
    const displaySubItem = { display: 'block' };
    const hideSubItem = { display: 'none' };

    return (
      <div className="sidebar sidebar-main">
        <div className="sidebar-content">
          <div className="sidebar-user">
            <div className="category-content">
              <div className="media">
                <a to="/posts" className="media-left">
                  <img
                    src="/assets/images/placeholder.jpg"
                    className="img-circle img-sm"
                    alt="spar"
                  />
                </a>
                <div className="media-body">
                  <span className="media-heading text-semibold">Admin</span>
                  <span className="media-heading text-semibold">After You Team</span>
                </div>

                <div className="media-right media-middle">
                  <ul className="icons-list">
                    <li>
                      <a to="/posts">
                        <i className="icon-cog3" />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="sidebar-category sidebar-category-visible">
            <div className="category-content no-padding">
              <ul className="navigation navigation-main navigation-accordion">
                <li className={this.state.active === 'Eat-in' ? activeStyle: ''}>
									<a className="has-ul" onClick={this.handleActive.bind(this, 'Eat-in')}><i className="icon-spell-check"></i> <span>Eat-in / Delivery</span></a>
									<ul className="hidden-ul" style={this.state.subactive === 'E-Order' || this.state.subactive === 'E-Product' || this.state.subactive === 'E-Product-Category' || this.state.subactive === 'E-Queue' || this.state.subactive === 'E-Branch' ? displaySubItem : hideSubItem}>
                  <li className={this.state.subactive === 'E-Order' ? activeStyle: ''}>
                      <NavLink to="/eatin/order" className={this.state.subactive === 'E-Order' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'E-Order')}>
                        <i className="icon-home4" /> <span>Order</span>
                      </NavLink>
                    </li>
										<li className={this.state.subactive === 'E-Product' ? activeStyle: ''}>
                      <NavLink to="/eatin/product" className={this.state.subactive === 'E-Product' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'E-Product')}>
                        <i className="icon-home4" /> <span>Product</span>
                      </NavLink>
                    </li>
                    <li className={this.state.subactive === 'E-Product-Category' ? activeStyle: ''}>
                      <NavLink to="/eatin/product/category" className={this.state.subactive === 'E-Product-Category' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'E-Product-Category')}>
                        <i className="icon-home4" /> <span>Product Category</span>
                      </NavLink>
                    </li>
                    <li className={this.state.subactive === 'E-Queue' ? activeStyle: ''}>
                      <NavLink to="/eatin/queue" className={this.state.subactive === 'E-Queue' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'E-Queue')}>
                        <i className="icon-home4" /> <span>Queue</span>
                      </NavLink>
                    </li>
                    <li className={this.state.subactive === 'E-Branch' ? activeStyle: ''}>
                      <NavLink to="/eatin/branch" className={this.state.subactive === 'E-Branch' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'E-Branch')}>
                        <i className="icon-home4" /> <span>Branch</span>
                      </NavLink>
                    </li>
									</ul>
								</li>
                <li className={this.state.active === 'Preorder' ? activeStyle: ''}>
									<a className="has-ul" onClick={this.handleActive.bind(this, 'Preorder')}><i className="icon-spell-check"></i> <span>Preorder / Online</span></a>
									<ul className="hidden-ul" style={this.state.subactive === 'P-Order' || this.state.subactive === 'P-Product' || this.state.subactive === 'P-Timelog' || this.state.subactive === 'P-Branch' ? displaySubItem : hideSubItem}>
                  <li className={this.state.subactive === 'P-Order' ? activeStyle: ''}>
                      <NavLink to="/preorder/order" className={this.state.subactive === 'P-Order' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'P-Order')}>
                        <i className="icon-home4" /> <span>Order</span>
                      </NavLink>
                    </li>
										<li className={this.state.subactive === 'P-Product' ? activeStyle: ''}>
                      <NavLink to="/preorder/product" className={this.state.subactive === 'P-Product' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'P-Product')}>
                        <i className="icon-home4" /> <span>Product</span>
                      </NavLink>
                    </li>
                    <li className={this.state.subactive === 'P-Branch' ? activeStyle: ''}>
                      <NavLink to="/preorder/branch" className={this.state.subactive === 'P-Branch' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'P-Branch')}>
                        <i className="icon-home4" /> <span>Branch</span>
                      </NavLink>
                    </li>
                    <li className={this.state.subactive === 'P-Timelog' ? activeStyle: ''}>
                      <NavLink to="/preorder/timelogs" className={this.state.subactive === 'P-Timelog' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'P-Timelog')}>
                        <i className="icon-home4" /> <span>Timelogs</span>
                      </NavLink>
                    </li>
									</ul>
								</li>
                <li className={this.state.active === 'Reward' ? activeStyle: ''}>
									<a className="has-ul" onClick={this.handleActive.bind(this, 'Reward')}><i className="icon-spell-check"></i> <span>Rewards</span></a>
									<ul className="hidden-ul" style={this.state.subactive === 'Earn' || this.state.subactive === 'R-Cate' || this.state.subactive === 'R-List' || this.state.subactive === 'Burn' ? displaySubItem : hideSubItem}>
                  <li className={this.state.subactive === 'Earn' ? activeStyle: ''}>
                      <NavLink to="/reward/earn" className={this.state.subactive === 'Earn' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'Earn')}>
                        <i className="icon-home4" /> <span>Earn</span>
                      </NavLink>
                    </li>
										<li className={this.state.subactive === 'R-Cate' ? activeStyle: ''}>
                      <NavLink to="/reward/category" className={this.state.subactive === 'R-Cate' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'R-Cate')}>
                        <i className="icon-home4" /> <span>Reward Category</span>
                      </NavLink>
                    </li>
                    <li className={this.state.subactive === 'R-List' ? activeStyle: ''}>
                      <NavLink to="/reward/list" className={this.state.subactive === 'R-List' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'R-List')}>
                        <i className="icon-home4" /> <span>Reward List</span>
                      </NavLink>
                    </li>
                    <li className={this.state.subactive === 'Burn' ? activeStyle: ''}>
                      <NavLink to="/reward/burn" className={this.state.subactive === 'Burn' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActiveSubmenu.bind(this, 'Burn')}>
                        <i className="icon-home4" /> <span>Burn Logs</span>
                      </NavLink>
                    </li>
									</ul>
								</li>
                <li className={this.state.active === 'News Feed' ? activeStyle: ''}>
                  <NavLink to="/news" className={this.state.active === 'News Feed' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActive.bind(this, 'News Feed')}>
                    <i className="icon-gear" /> <span>News</span>
                  </NavLink>
                </li>
                <li className={this.state.active === 'Member' ? activeStyle: ''}>
                  <NavLink to="/member" className={this.state.active === 'Member' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActive.bind(this, 'Member')}>
                    <i className="icon-user" /> <span>Member</span>
                  </NavLink>
                </li>
                <li className={this.state.active === 'Admin' ? activeStyle: ''}>
                  <NavLink to="/admin" className={this.state.active === 'Admin' ? 'navbar-item disabled-btn': 'navbar-item'} onClick={this.handleActive.bind(this, 'Admin')}>
                    <i className="icon-user" /> <span>Admin</span>
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
